package PathFinding;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Fantasma extends PhysicBody {

	public Fantasma(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub
		if(sprite instanceof PacMan) {
			sprite.delete();
			System.out.println("això no compila, el format de les dates està malament\r\n" + 
					":P");
		}
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}
	
	public void move(PacMan p) {
		//pathfinding debe haber margen
		
		System.out.println(chocoConAlgo(p,x1,y1,x2,y2));
		int pxcenter = (p.x1+p.x2)/2;
		int pycenter = (p.y1+p.y2)/2;
		int fxcenter = (x1+x2)/2;
		int fycenter = (y1+y2)/2;
		if(!isNear(p,'x') && pxcenter<fxcenter) {
			if(!chocoConAlgo(p,x1,y1,x2,y2)) {
				this.setVelocity(-2, 0);
			}else {
				//tenemos unestro objetivo a la izquierda pero hay un obstaculo
				//cuidado con esto que puede ser un bucle infinito
				int contador = 0;
				while(chocoConAlgo(p, x1, y1-contador, x2, y2-contador) && chocoConAlgo(p, x1, y1+contador, x2, y2+contador)) {
					contador++;
					//System.out.println(contador);
				}
				if(!chocoConAlgo(p, x1, y1-contador, x2, y2-contador)) {
					this.setVelocity(0, -2);
				}else {
					this.setVelocity(0, 2);
				}
			}
			
		}else if(!isNear(p,'x') && pxcenter>fxcenter && !chocoConAlgo(p,x1,y1,x2,y2)) {
			this.setVelocity(2, 0);
		}else if(!isNear(p,'y') && pycenter>fycenter) {
			this.setVelocity(0, 2);
		}else if(!isNear(p,'y') && pycenter<fycenter) {
			this.setVelocity(0, -2);
		} 
	}
	
	public boolean isNear(PacMan p,char c) {
		int MARGIN = 6;
		int pxcenter = (p.x1+p.x2)/2;
		int pycenter = (p.y1+p.y2)/2;
		int fxcenter = (x1+x2)/2;
		int fycenter = (y1+y2)/2;
		if(c=='x') {
			//si esta fora del marge
			if(pxcenter<fxcenter-MARGIN || pxcenter>fxcenter+MARGIN) {
				return false;
			}else {
				return true;
			}
		}else if(c=='y') {
			//si esta fora del marge
			if(pycenter<fycenter-MARGIN || pycenter>fycenter+MARGIN) {
				return false;
			}else {
				return true;
			}
		}
		return false;
	}
	
	public boolean chocoConAlgo(PacMan p, int fvx1, int fvy1, int fvx2, int fvy2) {
		int pxcenter = (p.x1+p.x2)/2;
		int pycenter = (p.y1+p.y2)/2;
		int fxcenter = (fvx1+fvx2)/2;
		int fycenter = (fvy1+fvy2)/2;
		if(p.x2<this.x1) {
			//esta a mi izquierda
			Sprite s = new Sprite("raycast",p.x2+2, fycenter-((fvy2-fvy1)/2)-2, fvx1-2, fycenter+((fvy2-fvy1)/2)+2,0,"",null);
			if(s.collidesWithField(this.f).isEmpty()) {
				return false;
			}else {
				return true;
			}
		}else if(p.x1>this.x2) {
			Sprite s = new Sprite("raycast",fvx2+2, fycenter-2, p.x1-2, fycenter+2,0,"",null);
			if(s.collidesWithField(this.f).isEmpty()) {
				return false;
			}else {
				return true;
			}
		}else if(pycenter>fycenter) {
			//esta a mi abajo
			this.setVelocity(0, 2);
		}else if(pycenter<fycenter) {
			//esta a mi arriba
			this.setVelocity(0, -2);
		} 
		return false;
	}
	
	

}
