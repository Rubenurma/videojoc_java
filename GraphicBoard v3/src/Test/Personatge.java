package Test;

import java.util.Set;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Personatge extends PhysicBody implements Constants{

	public boolean aterra;
	private int cont;
	
	public Personatge(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		cont++;
		System.out.println("Xoca "+sprite.name+" ("+cont + ") % "+this.collidesWithPercent(sprite));
		aterra = true;
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		System.out.println("No Xoca "+sprite.name+" ("+cont + ") % "+this.collidesWithPercent(sprite));
		aterra = false;		
	}

	@Override
	public void onCollisionStay(Sprite sprite) {		
		System.out.println(".. Xoca "+sprite.name+" ("+cont + ") % "+this.collidesWithPercent(sprite));
	}

	public void move(Set<Character> keys) {
		if(keys.contains('d')) {
			this.setVelocity(2, this.velocity[1]);
			flippedX=true;
		}else if(keys.contains('a')){
			this.setVelocity(-2, this.velocity[1]);
			flippedX=false;
		}else {
			this.setVelocity(0, this.velocity[1]);
		}	
	}

	public void jump() {
		this.addForce(0, -2);
	}
}
